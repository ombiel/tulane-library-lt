
var gulp = require("gulp");
var aek = require("@ombiel/aek-cli");
var buildtools = require("@ombiel/aek-buildtools");
var aekGulp = buildtools.gulp;
var screenMiddleware = require("@ombiel/aek-buildtools/screen-middleware");
var predeploy = aekGulp.predeploy({gulp:gulp,buildTask:"build"});
var path = require("path");

/* ---- SETTINGS ---------- */

var webpackOptions = {
  srcBase:path.resolve(__dirname,"src/js/"),
  dest:path.resolve(__dirname,".build/public/js"),
  externals:{"react":"React"},
  recordHash:false,
  alias:{
    "-components":"@ombiel/aek-lib/react/components",
    "-aek":"@ombiel/aek-lib"
  }
};

var screenOptions = {
  srcBase:path.resolve(__dirname,"src/screens/"),
  dest:path.resolve(__dirname,".build/screens/")
};

buildtools.webpackMiddleware.setOptions(webpackOptions);
screenMiddleware.setOptions(screenOptions);


/* ---- GULP TASKS ---------- */

gulp.task("webpack_prod",function() { return aekGulp.webpack(true,webpackOptions); });
gulp.task("screens_prod",screenMiddleware.gulp({env:"prod"}));
gulp.task("build",["webpack_prod","screens_prod"]);
gulp.task("auth",function() { return aek.cli.processCommand("auth",{silent:true}); });
gulp.task("runserver",["auth"],function() { return aek.cli.processCommand("runserver",{}); });

// predeloy task is triggered from `aek deploy` command
gulp.task("_predeploy",predeploy);

// default gulp task
gulp.task("default",["runserver"], function() {
  buildtools.webpackMiddleware.start();
});



// must export this for use with aek-cli
module.exports = gulp;
