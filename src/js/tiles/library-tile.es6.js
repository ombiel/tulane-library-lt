var {LiveTile,registerTile,getLibraries} = require("@ombiel/exlib-livetile-tools");
var libraryCSS = require("../../css/library");
var screenLink = require("@ombiel/aek-lib/screen-link");
var _get = require("-aek/utils").get;

// Declare variables for libraries scoped to this module
var $,_;

// Create a promise that resolves when these libraries are ready to use
var libsReady = getLibraries(["jquery","lodash"]).then(function(libs) {
  [$,_] = libs; // Assign the returned libraries to the global scope for this module
});

class LibraryTile extends LiveTile {

  onReady(){

    this.setOptions();

    libsReady.then(()=>{

      this.renderImmediately = this.render.bind(this);
      this.render = _.throttle(this.render.bind(this),1000); // Debounce render so it doesn't get triggered more than once in a 1 sec period

      this.fetchData();
      this.showOriginalFace();

    });

  }

  fetchData(){
    var url = screenLink("tulane-livestats-cmauth-lt/data");
    return this.ajax({url: url})
    .then((data)=>{
      // Check to ensure data is being returned before rendering
      if(data && data.stats) {
       this.response = data;
       this.render();
      }
    })
    .always(()=>{
      // Refresh data based on set delay
      if(this.fetchTimer) {
        this.fetchTimer.stop();
      }
      this.fetchTimer = this.timer(this.refreshDelay,this.fetchData.bind(this));
    });
  }

  render(){

    var content;
    var response = _get(this,"response.stats.stat",false);

    // Tile Attributes
    var tileAttributes = this.getTileAttributes();

    // Tile background image set in runserver.yaml
    if(tileAttributes.image || tileAttributes.img) {
      var image = tileAttributes.image || tileAttributes.img;
    }

    if(!response || !response.length) {

      // If there is no response the service returns an error desc
      var error = this.response.desc ? this.response.desc : "";

      // Do not show badges if there is no response data
      content = `
        <div class="${libraryCSS.libraryTile}" style="background-repeat: no-repeat; background-size: auto; background-position: center center; width: 100%; height: 100%; background-image: url(${image})">
          <div class="${libraryCSS.libraryTileInfo}">
            <p>${error}</p>
          </div>
        </div>
      `;

    }
    else {

      var loans = response[0] ? response[0] : false;
      var overdue = response[1] ? response[1] : false;
      var fees = response[2] ? response[2] : false;

      // If above values are not in response show a dash symbol
      if(!loans) {
        loans.value = "-";
      }
      if(!overdue) {
        overdue.value = "-";
      }
      if(!fees) {
        fees.value = "-";
      }

      var feeClass = `${libraryCSS.infoBox} ${libraryCSS.feesCon}`;
      var loanClass = `${libraryCSS.infoBox} ${libraryCSS.loansCon}`;
      var overClass = `${libraryCSS.infoBox} ${libraryCSS.overdueCon}`;

      var feeCircleClass = `${libraryCSS.infoCircle} ${libraryCSS.feeCircle}`;
      var loanCircleClass = `${libraryCSS.infoCircle} ${libraryCSS.loansCircle}`;
      var overCircleClass = `${libraryCSS.infoCircle} ${libraryCSS.overDueCircle}`;

      if(parseInt(fees.value) >= 0){
        feeCircleClass += ` ${libraryCSS.gotStuff}`;
      }
      if(parseInt(loans.value) >= 0){
        loanCircleClass += ` ${libraryCSS.gotStuff}`;
      }
      if(parseInt(overdue.value) >= 0){
        overCircleClass += ` ${libraryCSS.gotStuff}`;
      }

      content = `
        <div class="${libraryCSS.libraryTile}" style="background-repeat: no-repeat; background-size: auto; background-position: center center; width: 100%; height: 100%; background-image: url(${image})">
          <div class="${libraryCSS.libraryTileInfo}">
            <div>
              <div class="${loanClass}">
                <div class="${loanCircleClass}">${loans.value}</div>
                <h4 class="${libraryCSS.infoTitle}">${this.loansText}</h4>
              </div>
            </div>
            <div>
              <div class="${feeClass}">
                <div class="${feeCircleClass}">${this.currencySymbol + parseFloat(fees.value).toFixed(2)}</div>
                  <h4 class="${libraryCSS.infoTitle}">${this.feesText}</h4>
                </div>
              </div>
            <div>
              <div class="${overClass}">
                <div class="${overCircleClass}">${overdue.value}</div>
                <h4 class="${libraryCSS.infoTitle}">${this.requestsText}</h4>
              </div>
            </div>
          </div>
        </div>
      `;
    }
    this.setFace(content);

  }

  setOptions(){
    var tileAttributes = this.getTileAttributes();
    var libraryOptions = tileAttributes.library || {};
    this.loansText = libraryOptions.loansText || "Loans";
    this.requestsText = libraryOptions.requestsText || "Requests";
    this.feesText = libraryOptions.feesText || "Fines";
    this.currencySymbol = libraryOptions.requestsText || "$";
    this.refreshDelay = parseInt(tileAttributes.refreshDelay) || 60000;
  }

}

registerTile(LibraryTile,"libraryTile");
